# React Personal Porfolio

this is a portfolio implementation built with a mix of today's technologies, such as React, Express, Bootstrap, and others.

a fresh start to introduce oneself to others through accessible media and good and modern when displayed.

an amazing experience when our profile is conveyed well to others, with various features available and integrated.

## Environment

- [Reactjs](https://reactjs.org/)
- [React Bootstrap](https://react-bootstrap.github.io/)

### Contributing

You can contribute to this site by submitting pull request.

### Preview

you can see the results of this repo through a [live demo](https://github.com/novaardiansyah/react-personal-portfolio),
hopefully you like it.

it may take longer to demonstrate the website the first time, please be patient and it will all show up soon.

### Status

![stages](https://img.shields.io/badge/stages-development-informational)
[![information](https://img.shields.io/badge/information-references-informational)](https://github.com/novaardiansyah/budget-management-app/blob/main/references.json)
![size](https://img.shields.io/github/repo-size/novaardiansyah/react-personal-portfolio?label=size&color=informational)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://github.com/novaardiansyah/react-personal-portfolio/blob/main/LICENSE)
[![last commit](https://img.shields.io/github/last-commit/novaardiansyah/react-personal-portfolio?label=last%20commit&color=informational)](https://github.com/novaardiansyah/budget-management-app/commits/main)
